from django.shortcuts import redirect, render
from booksapp.forms import BookForm, MagazineForm
from booksapp.models import Book, Magazine, Genre 
from django.shortcuts import get_object_or_404

# Create your views here.

def show_books(request):
    books = Book.objects.all()
    context = {
        "books": books
    }
    return render(request, "books/list.html", context)

def create_book(request):
    context ={}

    form = BookForm(request.POST or None)
    if form.is_valid():
        book = form.save()
        return redirect("detail", pk=book.pk)

    context['form'] = form
    return render(request, "books/create.html", context)



def show_a_book(request, pk):
    context = {
        "book": Book.objects.get(pk=pk),
    }
    return render(request, "books/detail.html", context)


def update_book(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = BookForm(request.POST or None, instance = obj)
    if form.is_valid():
        book = form.save()
        return redirect("detail", pk=pk)
    context["form"] = form

    return render(request, "books/update_book.html", context)


def show_magazines(request):
    magazines = Magazine.objects.all()
    context = {
        "magazines": magazines
    }
    return render(request, "magazines/mlist.html", context)

def create_magazine(request):
    context ={}

    form = MagazineForm(request.POST or None)
    if form.is_valid():
        magazine = form.save()
        return redirect("show_magazines", pk=magazine.pk)

    context['form'] = form
    return render(request, "magazines/create_magazine.html", context)

def show_a_magazine(request, pk):
    context = {
        "magazine": Magazine.objects.get(pk=pk) if Magazine else None,
        # "genre": Genre.objects.get(pk=pk) if Genre else None,
        # "issue": Issue.objects.get(pk=pk) if Issue else None,


    }
    return render(request, "magazines/mdetail.html", context) 

def update_magazine(request, pk):
    context = {}
    obj = get_object_or_404(Book, pk=pk)
    form = MagazineForm(request.POST or None, instance = obj)
    if form.is_valid():
        book = form.save()
        return redirect("mdetail", pk=pk)
    context["form"] = form

    return render(request, "magazines/update_magazine.html", context)






def delete_magazine(request, pk):
	context ={}
	obj = get_object_or_404(Magazine, pk = pk)
	if request.method == "POST":
		obj.delete()
		return redirect("show_magazines")

	return render(request, "magazines/delete_magazine.html", context)

def genre(request, pk):
    context = {
        "genre": genre


    }

    return render(request, "magazines/genre.html", context)


def magazine_genre(request, pk):
    genre = Genre.objects.get(pk=pk)
    context = {

    }

    return render(request, "magazines/magazine_genre.html")



    
    # context = {}
    # obj = get_object_or_404(id)
    # form = CreateBook(repost.POST or None, instance = obj)
    # if form.is_valid():
    #     book = form.save()
    #     return redirect("show_a_book")
    # context["form"] = form
    # return render(request, "books/update_book.html", context)