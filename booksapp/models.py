from operator import ge
from django.db import models




    
    

# def __str__(self):
#     return self.name



class Book(models.Model):
    name = models.CharField(max_length=200, unique=True)
    author = models.CharField(max_length=100, null=True)
    num_pages = models.IntegerField(null=True)
    isbn_num = models.IntegerField(null=True)
    cover = models.URLField(null=True, blank=True)
    inprint = models.BooleanField(null=True)
    publish_date = models.IntegerField(null=True)


    def __str__(self):
        return self.name + " by " + self.author


fields = [
    "title",
    "author",
    "year_published"
]


class Magazine(models.Model):
    magazine_name = models.CharField(max_length=200, null=True)
    magazine_author = models.CharField(max_length=200, null=True)
    release_cycle = models.TextField(max_length=200, unique=True)
    description = models.CharField(max_length=100, null=True)
    cover = models.URLField(null=True, blank=True)
    # issue_number = models.ManytoManyField(Issue, related_name = "magazine")
    # page_count = models.CharField(null=True, blank=True)
    genres = models.ManyToManyField("Genre", related_name="magazines")

    def __str__(self):
        return self.magazine_name + " by " + self.magazine_author



class Issue(models.Model):
    name = models.CharField(max_length=200, unique=True)
    description = models.CharField(max_length=200, unique=True)
    cover = models.URLField(null=True, blank=True)
    date = models.IntegerField(null=True)
    # page_count = models.IntegerField(null=True)
    # issue_number = models.SmallIntegerField(null=True)
    magazine = models.ForeignKey(Magazine, related_name="issues", on_delete=models.CASCADE)


    def __str__(self):
        return self.name

class Genre(models.Model):
    genre_name = models.CharField(max_length=50)
    # magazine_name = models.ManyToManyField("Magazine", related_name="genres")



    def __str__(self):
        return self.genre_name

class  Author(models.Model):
    name = models.CharField(max_length=200, unique=True)

    def __str__(self):
        return self.name



class  BookReview(models.Model):
    book = models.ForeignKey(Book, related_name="reviews", on_delete=models.CASCADE)
    text = models.TextField()