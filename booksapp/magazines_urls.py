from django.contrib import admin
from django.urls import path
from booksapp.views import show_magazines, create_magazine, show_a_magazine, update_magazine, delete_magazine
from venv import create

urlpatterns = [
    path("create/", create_magazine, name="create_magazine"),
    path("", show_magazines,  name="show_magazines"),
    path("create/", show_a_magazine, name = "create_magazine"),
    path("<int:pk>/", show_a_magazine, name="mdetail"),
    path("<int:pk>/update_magazine/", update_magazine, name="update_magazine"),
    path("<int:pk>/delete/", delete_magazine, name="delete_magazine"),
    path("magazines/", show_magazines, name="show_magazines"),


]