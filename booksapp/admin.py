from django.contrib import admin
from booksapp.models import Book, Magazine, BookReview, Author, Genre, Issue
# Register your models here.

# class BookAdmin(admin.ModelAdmin):
#     readonly_fields = ('cover_preview',)

#     def cover_preview(self, obj):
#         return obj.cover_preview


admin.site.register(Book)
admin.site.register(Magazine)
admin.site.register(Author)
admin.site.register(BookReview)
admin.site.register(Genre)
admin.site.register(Issue)


