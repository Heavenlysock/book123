from django.contrib import admin
from django.urls import path
from booksapp.views import show_books, create_book, show_a_book, update_book
from venv import create

urlpatterns = [
    path("create/", create_book, name="create_book"),
    path("", show_books,  name="show_books"),
    path("create/", create_book, name = "create_book"),
    path("<int:pk>/", show_a_book, name="detail"),
    path("<int:pk>/update_book/", update_book, name="update_book"),
    # path('<id>/delete', delete_book, name="delete_book")
    path("books/list/", show_books,  name="show_books"),


]
