# Generated by Django 4.0.6 on 2022-07-19 23:20

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('booksapp', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='book',
            old_name='image',
            new_name='cover',
        ),
        migrations.RenameField(
            model_name='book',
            old_name='in_print',
            new_name='inprint',
        ),
        migrations.RenameField(
            model_name='book',
            old_name='title',
            new_name='name',
        ),
        migrations.RemoveField(
            model_name='book',
            name='description',
        ),
        migrations.RemoveField(
            model_name='book',
            name='isbn',
        ),
        migrations.RemoveField(
            model_name='book',
            name='pages',
        ),
        migrations.RemoveField(
            model_name='book',
            name='year_published',
        ),
        migrations.AddField(
            model_name='book',
            name='isbn_num',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='book',
            name='num_pages',
            field=models.IntegerField(null=True),
        ),
        migrations.AddField(
            model_name='book',
            name='publish_date',
            field=models.IntegerField(null=True),
        ),
        migrations.AlterField(
            model_name='book',
            name='author',
            field=models.CharField(max_length=100, null=True),
        ),
    ]
