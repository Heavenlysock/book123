from django import forms
from .models import Book, Magazine


class BookForm(forms.ModelForm):
    class Meta:
        model = Book
        exclude = []


class MagazineForm(forms.ModelForm):
    class Meta:
        model = Magazine
        exclude = []



